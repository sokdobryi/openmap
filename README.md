Build and run the both postgres and the FastAPI webapp:
```
docker compose up --build
```
To start tests:
```
env DEBUG=test docker compose up --build
```


Open the api documentation at http://localhost:8000/docs


To clean up the database:
```
docker compose down -v
```

to clean all:
```
docker system prune --all --volumes 
```

