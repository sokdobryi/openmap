import os
import logging
from datetime import timedelta
from fastapi.security import OAuth2PasswordRequestForm
from starlette.status import HTTP_302_FOUND
import app.db_models
from typing import Optional
from starlette import status
from starlette.responses import FileResponse, JSONResponse
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
from fastapi import FastAPI, Request, Response, UploadFile, File, Depends, Form, status, Header, Body, HTTPException
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse
import json
from app.auth import *
from app.db_connect import engine
from app.shemas import *
from app.objects import *

UPLOADED_IMG_PATH = "./static/img/todos"

server = FastAPI()
app.db_models.Base.metadata.create_all(engine)
os.chdir("app")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="../main.log")
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))

templates = Jinja2Templates(directory="templates")
server.mount("/app/static", StaticFiles(directory="static"), name="static")


@server.get("/", tags=["Main"])
async def home(request: Request, access_token: str = ''):
    """
    Function for main page
    :param request: request
    :return: HTML of main page 
    """
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/\" status=\"HTTP_200_OK\"')
    if access_token:
        return templates.TemplateResponse("index.html", {"request": request})
    else:
        return templates.TemplateResponse("enter.html", {"request": request})
    

@server.get("/map", tags=["Map"])
async def draw_map(request: Request, access_token: str = ''):
    """
    Function for drawing map
    """
    return templates.TemplateResponse("map.html", {"request": request})


@server.get("/admin", tags=["Admin"])
async def draw_map(request: Request, access_token: str = ''):
    """
    Function for drawing map
    """

    return templates.TemplateResponse("admin.html", {"request": request,
                                                     "cams": get_all_cams()})


@server.get("/show_cams", tags=["Map"])
async def show_cams():
    """
    Function for sending info about cameras
    :return: Json info about cameras
    """
    try:
        with open("static/cams.json", "r") as file:
            cameras = json.load(file)
        return JSONResponse(content=cameras)
    except FileNotFoundError:
        raise HTTPException(status_code=500, detail="File not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@server.get("/show_cons", tags=["Map"])
async def show_cams():
    """
    Function for sending info about consul
    :return: Json info about consul
    """
    try:
        with open("static/cons.json", "r") as file:
            cons = json.load(file)
        return JSONResponse(content=cons)
    except FileNotFoundError:
        raise HTTPException(status_code=500, detail="File not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@server.get("/show_kur", tags=["Map"])
async def show_cams():
    """
    Function for sending info about consul
    :return: Json info about consul
    """
    try:
        with open("static/kur.json", "r") as file:
            cons = json.load(file)
        return JSONResponse(content=cons)
    except FileNotFoundError:
        raise HTTPException(status_code=500, detail="File not found")
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@server.post("/add_info", tags=["Admin"])
async def add_info_CLI(request: Request,
                       lat: str = Header(...), 
                       lan: str = Header(...), 
                       selectType: str = Header(...),
                       serial: str = '', 
                       optional: str = '',
                       name: str = '',
                       phone: str = '',
                       #token: str = Header(...)
                       ):
    """
    Function to create info
    """
    #user = get_current_user(token)
    if selectType == 'cam':
        return create_cam(lat, lan, serial, optional)
    elif selectType == 'kur':
        return "kur"
        #create_kur(lat, lan, name, phone)
    elif selectType == 'cons':
        return selectType
        #create_cons(lat, lan, name)
    else:
        logger.warning(f'client=\"{request.client.host}:{request.client.port}\" username: {user.username} bad_request to create info')
    return selectType


@server.get("/cams", tags=["Admin"])
def get_cams():
    return get_all_cams()

@server.post("/register-user", tags=["Auth"])
def registration_new_user(username: str = Header(...), password: str = Header(...)):
    url = server.url_path_for('home')
    user = get_user(username)
    if user:
        raise HTTPException(status_code=400, detail="Username already registered")
    user = create_user(username, password)
    return user


@server.get("/user", tags=["Auth"])
async def get_user_info(token: str = Header(...)):
    user = get_current_user(token)
    return user


@server.post("/token", tags=["Auth"])
async def login_for_access_token(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return access_token
