async function fetchUser() {
    const response = await fetch("/user", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "token": token,
        },
    })
    const jsonData = await response.json();
    const username = jsonData.username;
    if ( username == undefined) {
        localStorage.removeItem('token');
        window.location.href="/";
    }
    const git_id = jsonData.git_id;
    const avatar_url = jsonData.avatar;
    document.getElementById('user_avka').src = avatar_url;
    const userRole = jsonData.role;
    if (userRole == "3") {
        document.getElementById('adminPanel').style.display = 'block';
    }
    document.getElementById('loginingUsername').innerHTML = username;
} 
