function addCamMarker(lat, lng, serial, optional) {
          const marker = L.marker([lat, lng], {icon: camMarkerIcon}).addTo(map);
          camMarkers.push(marker);
          const popupContent = 'serial: ' + serial + '<br> optional: ' + optional;
          marker.bindPopup(popupContent);
        }

function showMarkers() {
  if (camMarkers.length > 0) {
    camMarkers.forEach(marker => map.removeLayer(marker));
    camMarkers = [];
  } else {
    fetch('/show_cams')
      .then(response => response.json())
      .then(data => {
        data.forEach(cam => {
          const lat = parseFloat(cam.s);
          const lng = parseFloat(cam.d);
          addCamMarker(lat, lng, cam.serial, cam.optional)
        });
      })
      .catch(error => console.error('Ошибка:', error));
  }
}

function addConsMarker(lat, lng) {
          const marker = L.marker([lat, lng], {icon: consMarkerIcon}).addTo(map);
          consMarkers.push(marker);
          const popupContent = 'Посольство США';
          marker.bindPopup(popupContent);
        }

function showCons() {
  if (consMarkers.length > 0) {
    consMarkers.forEach(marker => map.removeLayer(marker));
    consMarkers = [];
  } else {
    fetch('/show_cons')
      .then(response => response.json())
      .then(data => {
        data.forEach(cam => {
          const lat = parseFloat(cam.s);
          const lng = parseFloat(cam.d);
          addConsMarker(lat, lng)
        });
      })
      .catch(error => console.error('Ошибка:', error));
  }
}

function addKurMarker(lat, lng) {
          const marker = L.marker([lat, lng], {icon: kurMarkerIcon}).addTo(map);
          kurMarkers.push(marker);
          const popupContent = 'Курируемый объект';
          marker.bindPopup(popupContent);
        }

function showKur() {
  if (kurMarkers.length > 0) {
    kurMarkers.forEach(marker => map.removeLayer(marker));
    kurMarkers = [];
  } else {
    fetch('/show_kur')
      .then(response => response.json())
      .then(data => {
        data.forEach(cam => {
          const lat = parseFloat(cam.s);
          const lng = parseFloat(cam.d);
          addKurMarker(lat, lng)
        });
      })
      .catch(error => console.error('Ошибка:', error));
  }
}

function clearMarkers() {
  alert('123');
}