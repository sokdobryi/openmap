function saveRoute(markers) {
    var coordinates = markers.map(marker => [marker.getLatLng().lat, marker.getLatLng().lng]);

    fetch('/routes/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: 'Маршрут',
            points: JSON.stringify(coordinates)
        })
    })
    .then(response => response.json())
    .then(data => {
        console.log('Маршрут сохранен:', data);
        // Здесь можно добавить код для обновления интерфейса или других действий после сохранения
    })
    .catch(error => {
        console.error('Ошибка при сохранении маршрута:', error);
    });
}

