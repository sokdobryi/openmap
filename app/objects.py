from app.db_connect import SessionLocal
from pydantic import BaseModel
from app.db_models import *


def create_cam(lat: str, lan: str, serial: str, optional:str):
	db = SessionLocal()
	create_cam = Camera(lat=lat, lan=lan, serial=serial, optional=optional)
	db.add(create_cam)
	db.commit()
	db.refresh(create_cam)
	db.close()
	return create_cam


def get_all_cams():
	db = SessionLocal()
	cams = db.query(Camera).all()
	db.close()
	return cams


def delete_cam_by_id(id: int):
	db = SessionLocal()
	post = db.query(app.db_models.Camera).filter(app.db_models.Camera.id == id).first()
	db.delete(post)
	db.commit()
    db.close()
