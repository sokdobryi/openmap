import enum
from datetime import datetime
from sqlalchemy import Column, Integer, String, null, Enum, Boolean, DateTime, ForeignKey, MetaData
from sqlalchemy.orm import relationship
from app.db_connect import Base


class Roles(enum.Enum):
    employee = 1
    admin = 2


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=False, index=True, nullable=True, default=null())
    hashed_password = Column(String, nullable=True, default=null())
    role = Column(Enum(Roles))


class Camera(Base):
    __tablename__ = "cameras"
    id = Column(Integer, primary_key=True, index=True)
    lat = Column(String, unique=False, nullable=True, default=null())
    lan = Column(String, unique=False, nullable=True, default=null())
    serial = Column(String, unique=True, index=True, nullable=True, default=null())
    optional = Column(String, unique=False, nullable=True, default=null())


class SupervisedObject(Base):
    __tablename__ = "supervised_objects"
    id = Column(Integer, primary_key=True, index=True)
    lat = Column(String, unique=False, nullable=True, default=null())
    lan = Column(String, unique=False, nullable=True, default=null())
    name = Column(String, unique=False, nullable=True, default="Курируемый объект")
    phone = Column(String, unique=False, nullable=True, default=null())


class Consulate(Base):
    __tablename__ = "consulates"
    id = Column(Integer, primary_key=True, index=True)
    lat = Column(String, unique=False, nullable=True, default=null())
    lan = Column(String, unique=False, nullable=True, default=null())
    name = Column(String, unique=False, nullable=True, default="Консульство")